<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name', 'description','categorie_id', 'price','image','establishment_id'
    ];

    public function establishment(){
        return $this->belongsTo('App\Establishment');
    }
}
