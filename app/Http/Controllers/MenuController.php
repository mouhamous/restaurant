<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use App\Menu;

use App\Categorie;


class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()){
            return [ 'categories' => Categorie::all(), 'menu' =>Menu::all()];
        }else{
            return redirect('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        if ($request->ajax()){
            $this->validate(request(), [
                'name' => ['required', 'string', 'max:255'],
                'description' => ['required'],
                'price' => ['required'],
                'categorie_id' => ['required'],
                'file' => 'required|mimes:jpeg,jpg,png',

            ]);
    
            
    
            $path = $request->file->store('menu');
            $request->merge([
                'image' => $path,"establishment_id"=> $user->establishment->id,
                ]) ;
    
    
            $response= Menu::create($request->all());
    
            return $response;

        }else{
            return redirect('/');
        }
        
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::find($id);
        
        if ($menu->establishment->user->id == Auth::user()->id){
            return [ 'categories' => Categorie::all(), 'menu' => $menu];
        }else{
            return redirect('admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        $data =(object) $request->menu;
        
        if ($menu->establishment->user ==Auth::user()){
            $menu->name = $data->name;
            $menu->description =$data->description;
            $menu->categorie_id = $data->categorie_id;
            $menu->price = $data->price;
            $menu->update();
            return $menu->id;
        }else{
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        
        if ($menu->establishment->user ==Auth::user()){
            $menu->delete();
            return $menu->id;
        }else{
            return abort(404);
        }
    }
}
