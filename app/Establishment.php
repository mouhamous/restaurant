<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Establishment extends Model
{
    protected $fillable = [
        'user_id', 'name', 'city'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
