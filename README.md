# Requires laravel & npm

# type commande 
composer install && composer dumpautoload

# Copy the environment template 
cp .env.example .env 

# Create database and user privilege

# generate key with command
php artisan key:generate

# clear cache 
php artisan config:cache

# Install npm dependency with
npm install 

# After run build 
npm run watch 

# Seeds
php artisan db:seed


# and run laravel server with
php artisan serve


