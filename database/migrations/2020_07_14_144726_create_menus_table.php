<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->mediumText('description');
            $table->float('price');
            $table->string('image');
            $table->integer('categorie_id')
                    ->foreign('categorie_id')
                    ->references('id')->on('categories')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->integer('establishment_id')
                    ->foreign('establishment_id')
                    ->references('id')->on('establishments')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
