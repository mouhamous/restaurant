<?php

use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories= ['Entrées', 'Pates', 'Pizzas', 'Plats', 'Desserts', 'Boissons'];
        foreach($categories as $categorie){
            DB::table('categories')->insert([
                'name' => $categorie,
                
            ]);
        }
        
    }
}
